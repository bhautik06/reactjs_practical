import React, { useState } from 'react';
// import Modal from 'react-modal'
// import Tippy from '@tippy.js/react'
// import CountUp, { useCountUp } from 'react-countup'
import './App.css';
// import 'tippy.js/dist/tippy.css'
// import IdleTimerContainer from './components/IdleTimerContainer';
import { ChromePicker } from 'react-color';
// import { toast } from 'react-toastify'
// import 'react-toastify/dist/ReactToastify.css'

// import { IconContext } from 'react-icons'
// import { FaReact } from 'react-icons/fa'
// import { MdAlarm } from 'react-icons/md'
// const CustomToast = ({ closeToast }) => {
//   return (
//     <div>
//       Something went wrong!!!
//       <button onClick={closeToast}></button>
//     </div>
//   )
// }

// toast.configure()
// Modal.setAppElement('#root')
// const ColoredTooltip = () => {
//   return <span style={{ color: 'yellow' }}>Colored Component</span>
// }

// const CustomChild = forwardRef((props, ref) => {
//   return (
//     <div ref={ref}>
//       <div>First line</div>
//       <div>Second line</div>
//     </div>
//   )
// })

function App() {
  // const { countUp, start, pauseResume, reset, update } = useCountUp({
  //   duration: 5,
  //   end: 10000,
  //   startOnMount: false
  // })
  // const notify = () => {
  //   toast('Basic notification!', { position: toast.POSITION.TOP_LEFT })
  //   toast.success('Success notification!', {
  //     position: toast.POSITION.TOP_CENTER,
  //     autoClose: 8000
  //   })
  //   toast.info('Info notification!', {
  //     position: toast.POSITION.TOP_RIGHT,
  //     autoClose: false
  //   })
  //   toast.warn(<CustomToast />, {
  //     position: toast.POSITION.BOTTOM_LEFT
  //   })
  //   toast.error('Error notification!', {
  //     position: toast.POSITION.BOTTOM_CENTER
  //   })
  //   toast('Basic notification!', {
  //     position: toast.POSITION.BOTTOM_RIGHT
  //   })
  // }
  // const [modalIsOpen, setModalIsOpen] = useState(false)

  const [color, setColor] = useState('#fff')
  const [showColorPicker, setShowColorPicker] = useState(false)

  return (
    // <IconContext.Provider value={{ color: 'blue', size: '5rem' }}>
    //   <div className="App">
    //     <FaReact />
    //     <MdAlarm color="purple" size="10rem" />
    //   </div>
    // </IconContext.Provider>
    // <div className="App">
    //   <button onClick={notify}>Notify!</button>
    // </div>
    // <div className="App">
    //   <button onClick={() => setModalIsOpen(true)}>Open modal</button>
    //   <Modal
    //     isOpen={modalIsOpen}
    //     shouldCloseOnOverlayClick={false}
    //     onRequestClose={() => setModalIsOpen(false)}
    //     style={
    //       {
    //         overlay: {
    //           backgroundColor: 'gray'
    //         },
    //         content: {
    //           color: 'orange'
    //         }
    //       }
    //     }
    //   >
    //     <h2>Modal Title</h2>
    //     <p> Modal Body</p>
    //     <div>
    //       <button onClick={() => setModalIsOpen(false)}>Close</button>
    //     </div>
    //   </Modal>
    // </div>
    <div>
      <button onClick={() => setShowColorPicker(showColorPicker => !showColorPicker)}>
        {showColorPicker ? 'Close color picker' : 'Pick a color'}
      </button>
      {showColorPicker && (
        <ChromePicker
          color={color}
          onChange={updatedColor => setColor(updatedColor.hex)}
        />
      )}

      <h2>You picked {color}</h2>
      {/* <IdleTimerContainer /> */}
      {/* <div style={{ paddingBottom: "20px" }}>
        <Tippy placement='right' arrow={false} delay={1000} content='Basic Tooltip'>
          <button>Hover</button>
        </Tippy>
      </div>

      <div style={{ paddingBottom: "20px" }}>
        <Tippy content={<span style={{ color: 'orange' }}>Colored</span>}>
          <button>Hover</button>
        </Tippy>
      </div>

      <div style={{ paddingBottom: "20px" }}>
        <Tippy content={<ColoredTooltip />}>
          <button>Hover</button>
        </Tippy>
      </div>

      <div style={{ paddingBottom: "20px" }}>
        <Tippy placement='top-start' content={<ColoredTooltip />}>
          <CustomChild />
        </Tippy>
      </div>

      <div>
        <h1>{countUp}</h1>
        <button onClick={start}>Start</button>
        <button onClick={reset}>Reset</button>
        <button onClick={pauseResume}>PauseResume</button>
        <button onClick={() => update(2000)}>Update to 2000</button>
      </div>
      <h1>
        <CountUp end={200} />
      </h1>
      <h1>
        <CountUp end={200} duration={5} />
      </h1>
      <h1>
        <CountUp start={500} end={1000} duration={5} />
      </h1>
      <h1>
        <CountUp end={1000} duration={5} prefix='$' decimals={2} />
      </h1>
      <h1>
        <CountUp end={1000} duration={5} suffix='USD' decimals={2} />
      </h1> */}


    </div>
  );
}

export default App;
